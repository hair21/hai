<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Basic Table</h2>
    <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>
    <a href="{{route('admin.admins.create')}}" class="btn btn-success">Thêm</a>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>

            <th>Ngày tạo</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($admins as $course)
            <tr>
                <td>{{$course->_id}}</td>
                <td>{{$course->name}}</td>
                <td>{{$course->created_at}}</td>
                <td>
                    <a href="{{route('admin.admins.edit',$course->_id)}}">Sửa</a>
                    <a href="{{route('admin.admins.show',$course->_id)}}">xem</a>
                    <form action="{{route('admin.admins.destroy',$course->_id)}}" method="post">
                        @csrf
                        @method('delete')
                        {{--<button type="submit">Xóa</button>--}}
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>