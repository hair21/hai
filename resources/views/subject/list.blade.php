<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Basic Table</h2>
    <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>
    <a href="{{route('admin.courses.create')}}" class="btn btn-success">Thêm</a>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Name</th>
            <th>Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($chapters as $chapter)
            <tr>
                <td>{{$chapter->_id}}</td>
                <td>{{$chapter->heading}} </td>
                <td>{{$chapter->name}} </td>
                <td>
                    <a href="{{route('admin.chapters.edit',$chapter->_id)}}">Sửa</a>
                    <form action="{{route('admin.chapters.destroy',$chapter->_id)}}" method="post">
                        @csrf
                        @method('delete')
                        {{--<button type="submit">Xóa</button>--}}
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>

</body>
</html>