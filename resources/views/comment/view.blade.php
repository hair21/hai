<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Basic Table</h2>
    <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>
    <form action="{{route('admin.comments.store')}}" method="post">
        <div class="row">
            @csrf
            <div class="col-md-2">
                <button type="submit" class="btn btn-success" style="width: 100%">Thêm</button>
                <select name="admin_id" class="form-control" id="">
                    @foreach($admins as $id=>$name)
                        <option value="{{$id}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-10">
                <textarea name="content" class="form-control" cols="30" rows="3"></textarea>
            </div>
        </div>
    </form>
</div>
{{--{{dd($commnets)}}--}}
<div class="container">
    <h4>Nested Media Objects</h4>

    @foreach($comments as $comment)
        <div class="media">
            <div class="media-left">
                <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" class="media-object" style="width:45px">
            </div>
            <div class="media-body">
                <h4 class="media-heading">{{$comment->admin->name}}
                    <small><i>{{$comment->created_at}}</i></small>
                </h4>
                <p>{{$comment->content}}.</p>

                <!-- Nested media object -->

                @if($comment->reply)

                    <hr>
                    <b>Có {{count($comment->repcommnet)}} reply :</b>

                    @foreach($comment->repcommnet as $repcommnet)
                        <div class="media">
                            <div class="media-left">
                                <img src="https://www.w3schools.com/bootstrap/img_avatar2.png" class="media-object"
                                     style="width:45px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">{{$repcommnet->admin->name}}
                                    <small><i>{{$repcommnet->created_at}}</i></small>
                                </h4>
                                <p>{{$repcommnet->content}}.</p>
                            </div>
                        </div>
                        <hr>
                    @endforeach
                @endif
                <div class="media">
                    <form action="{{route('admin.comments.store')}}" method="post">
                        <div class="row">
                            @csrf
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-success" style="width: 100%">Reply</button>
                                <input type="hidden" name="comment_id" value="{{$comment->_id}}">
                                <select name="admin_id" class="form-control" id="">
                                    @foreach($admins as $id=>$name)
                                        <option value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-10">
                                <textarea name="content" class="form-control" cols="30" rows="3"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
</div>
</body>
</html>