<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    {{--<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>--}}
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML"></script>

</head>
<body>
{{--<div id="MathJax_Message"></div>--}}
<div class="container">
    <h2>{{$section->chapter->name}}</h2>
    <h3>{{@$section->lesson->name}}</h3>
    <h4 class="text-center">{{$section->name}}</h4>
    Câu hỏi :
    <br>
    {!! $section->question !!}
    Câu trả lời :
    <br>
    {!! $section->answer !!}

</div>

</body>
</html>