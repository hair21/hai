<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Form control: input</h2>
    <p>The form below contains two input elements; one of type text and one of type password:</p>
    <form action="{{route('admin.clones.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="usr">Admin:</label>
            <select name="class" class="form-control" id="">
                <option value="6">Lop 6</option>
                <option value="7">Lop 7</option>
                <option value="8">Lop 8</option>
                <option value="9">Lop 9</option>
                <option value="10">Lop 10</option>
                <option value="11">Lop 11</option>
                <option value="12">Lop 12</option>
            </select>
        </div>
        <div class="form-group">
            <label for="usr">ID:</label>
            <input type="text" class="form-control" name="id_clone" id="usr">
        </div>
        <div class="form-group">
            <label for="usr">Name:</label>
            <input type="text" class="form-control" name="name">
            {{--<select name="name" class="form-control" id="">--}}
            {{--<option>Ngữ văn</option>--}}
            {{--<option>Toán học</option>--}}
            {{--<option>Vật lí</option>--}}
            {{--<option>Sinh học</option>--}}
            {{--<option>Tiếng anh</option>--}}
            {{--<option>Lịch sử</option>--}}
            {{--<option>Địa lí</option>--}}
            {{--<option>GDCD</option>--}}
            {{--</select>--}}
        </div>
        <button class="btn btn-success" type="submit">Thêm mới</button>
    </form>
</div>

</body>
</html>