<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Basic Table</h2>
    <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>
    <a href="{{route('admin.clones.create')}}" class="btn btn-success">Thêm</a>
    <table class="table">
        <thead>
        <tr>
            <th>Lớp</th>
            <th>ID Clone</th>
            <th>Status</th>
            <th>name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clones as $class=>$clone)
            @foreach($clone as $key=>$id)
                <tr>
                    @if($key == 0)
                        <td rowspan="{{count($clone)}}">Lop {{$class}}</td>
                    @endif
                    <td>{{$id->id_clone}}</td>
                    <td>{{$id->status}}</td>
                    <td>{{$id->name}}</td>
                    <td>
                        <a href="{{route('admin.clones.edit',$id->id)}}">Sửa</a>
                        <form action="{{route('admin.clones.destroy',$id->id)}}" method="post">
                            @csrf
                            @method('delete')
                            {{--<button type="submit">Xóa</button>--}}
                        </form>
                    </td>
                </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>