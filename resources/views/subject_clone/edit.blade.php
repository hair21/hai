<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Form control: input</h2>
    <p>The form below contains two input elements; one of type text and one of type password:</p>
    <form action="{{route('admin.clones.update',$clone->id)}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="usr">ID:</label>
            <input type="text" class="form-control" value="{{$clone->id_clone}}" name="id_clone" id="usr">
        </div>

        <button class="btn btn-success" type="submit">Thêm mới</button>
    </form>
</div>

</body>
</html>