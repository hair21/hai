<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Basic Table</h2>
    <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>
    <a href="{{route('admin.courses.create')}}" class="btn btn-success">Thêm</a>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subjects as $subject)
            <tr>
                <td>{{$subject->_id}}</td>
                <td>{{$subject->name}} </td>
                <td>
                    <a href="{{route('admin.subject.list',['subject_id'=>$subject->_id,'course_id'=>$course->_id])}}">Sửa</a>
                    {{--<a href="{{route('admin.subjects.list',$subject->_id)}}">Xem</a>--}}
                    <form action="{{route('admin.subjects.destroy',$subject->_id)}}" method="post">
                        @csrf
                        @method('delete')
                        {{--<button type="submit">Xóa</button>--}}
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>