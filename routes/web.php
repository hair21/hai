<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin', 'as' => 'admin.', 'prefix' => 'admin'], function () {
    Route::resource('comments', 'CommentController');
    Route::resource('clones', 'CloneController');
    Route::resource('courses', 'CourseController');
    Route::resource('chapters', 'ChapterController');
    Route::resource('lessons', 'LessonController');
//    Route::get('section/{id}/', 'LessonController@list')->name('section.list');
//    Route::resource('subjects', 'SubjectController');
    Route::delete('subjects/{id}', 'SubjectController@destroy')->name('subjects.destroy');
    Route::get('chapter-list/{course_id}/{subject_id}', 'SubjectController@listchap')->name('subject.list');
    Route::get('subject-list/{id}', 'SubjectController@list')->name('subjects.list');
    Route::resource('admins', 'AdminController');
    Route::resource('sections', 'SectionController');
    Route::resource('chapters', 'ChapterController');
    Route::get('list-sections/{id_lesson}', 'SectionController@findLesson')->name('lessons.list');
    Route::get('list-quiz/{id_lesson}', 'SectionController@quiz')->name('lessons.quiz');
    Route::post('download-quiz/{id_lesson}', 'SectionController@download')->name('section.post');
    Route::get('download-quiz/{id_lesson}', 'SectionController@download')->name('section.download');
    Route::get('li-thuyet/{id_lesson}', 'ChapterController@lithuyet')->name('lessons.lithuyet');
});