<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateLessonsTable.
 */
class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->dropIfExists('lessons');
        Schema::connection('mysql')->create('lessons', function (Blueprint $table) {
            $table->bigIncrements('_id');
            $table->string('name');
            $table->string('sub_title')->nullable();
            $table->string('slug');
            $table->text('description')->nullable();
            $table->unsignedBigInteger('chapter_id');
            $table->foreign('chapter_id')->references('_id')->on('chapters')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lessons');
    }
}
