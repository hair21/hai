<?php

use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSectionsTable.
 */
class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::connection('mongodb')->create('sections', function ($collection) {
            $collection->index('lesson_id');
            $collection->index('chapter_id');
            $collection->index('name');
            $collection->index('type');
            $collection->index('answer');
            $collection->index('question');
            $collection->index('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sections');
    }
}
