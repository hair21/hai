<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateSubjectsTable.
 */
class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->dropIfExists('subjects');
        Schema::connection('mysql')->create('subjects', function (Blueprint $table) {
            $table->bigIncrements('_id');
            $table->string('name');
            $table->string('slug');
            $table->text('description')->nullable();
//            $table->unsignedBigInteger('course_id');
//            $table->foreign('course_id')->references('_id')->on('courses')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subjects');
    }
}
