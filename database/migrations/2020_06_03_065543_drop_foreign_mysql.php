<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignMysql extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//  dung 2 database nen xoa khoa ngoai de chay migrate:refresh
//        if (Schema::connection('mysql')->hasTable('subjects')) {
//            Schema::connection('mysql')->table('subjects', function (Blueprint $table) {
//                $table->dropForeign('subjects_course_id_foreign');
//            });
//        }
        if (Schema::connection('mysql')->hasTable('chapters')) {
            Schema::connection('mysql')->table('chapters', function (Blueprint $table) {
                $table->dropForeign('chapters_course_id_foreign');
                $table->dropForeign('chapters_subject_id_foreign');
            });
        }
        if (Schema::connection('mysql')->hasTable('lessons')) {
            Schema::connection('mysql')->table('lessons', function (Blueprint $table) {
                $table->dropForeign('lessons_chapter_id_foreign');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
