<?php

use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAnswerItemsTable.
 */
class CreateAnswerItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('answer_items', function ($collection) {
            $collection->index('quiz_id');
            $collection->index('content');
            $collection->index('is_correct');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answer_items');
    }
}
