<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateChaptersTable.
 */
class CreateChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->dropIfExists('chapters');
        Schema::connection('mysql')->create('chapters', function (Blueprint $table) {
            $table->bigIncrements('_id');
            $table->string('name');
            $table->string('short_name');
            $table->string('heading')->nullable();
            $table->string('slug');
            $table->text('description')->nullable();
            $table->bigInteger('course_id')->unsigned();
            $table->foreign('course_id')->references('_id')->on('courses')->onDelete('cascade');
            $table->bigInteger('subject_id')->unsigned();
            $table->foreign('subject_id')->references('_id')->on('subjects')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chapters');
    }
}
