<?php

use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateQuizzesTable.
 */
class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('quizzes', function ($collection) {
            $collection->index('question');
            $collection->index('answer');
            $collection->index('quest_id');
            $collection->index('lesson_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quizzes');
    }
}
