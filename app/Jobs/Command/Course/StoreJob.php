<?php

namespace App\Jobs\Command\Course;

use App\Entities\Course;
use App\Jobs\Job;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\CourseRepositoryEloquent;
use Illuminate\Http\Request;

class StoreJob extends Job
{
    protected $course;

    /**
     * StoreJob constructor.
     * @param Request $request
     */
    public function __construct($course)
    {
        $this->course = $course;
    }

    /**
     * @param CourseRepositoryEloquent $repositoryEloquent
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(CourseRepositoryEloquent $repositoryEloquent)
    {
        $course = Course::where('name', 'Lớp ' . $this->course)->first();
        if (!is_object($course)) {
            $course = $repositoryEloquent->create([
                'name' => 'Lớp ' . $this->course,
                'slug' => str_slug('Lớp ' . $this->course, '-'),
            ]);
        }
        return $course;
    }
}
