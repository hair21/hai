<?php

namespace App\Jobs\Command\Quiz;

use App\Entities\Quiz;
use App\Jobs\Job;
use App\Repositories\AnswerItemRepositoryEloquent;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\QuizRepositoryEloquent;

class StoreJob extends Job
{
    protected $obj_question;
    protected $lesson_id;

    /**
     * StoreJob constructor.
     * @param $obj_question
     * @param $lesson_id
     */
    public function __construct($obj_question, $lesson_id)
    {
        $this->obj_question = $obj_question;
        $this->lesson_id = $lesson_id;
    }

    /**
     * @param QuizRepositoryEloquent $quizRepositoryEloquent
     * @param AnswerItemRepositoryEloquent $answerItemRepositoryEloquent
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(QuizRepositoryEloquent $quizRepositoryEloquent, AnswerItemRepositoryEloquent $answerItemRepositoryEloquent)
    {
        $item_question = $this->obj_question;
        $quiz = Quiz::where('quest_id', (int)$item_question->quest_id)->first();
        if (!is_object($quiz)) {
            $quiz = $quizRepositoryEloquent->create([
                'question' => $item_question->quest_name,
                'quest_id' => (int)$item_question->quest_id,
                'lesson_id' => (int)$this->lesson_id
            ]);
            try {
                foreach ($item_question->quest_answer as $quest_answer) {
                    $answerItemRepositoryEloquent->create([
                        'quiz_id' => $quiz->_id,
                        'content' => $quest_answer->answer_name,
                        'is_correct' => (int)$quest_answer->answer_iscorrect,
                    ]);
                }
            } catch (\Exception $exception) {
                echo 'Error : Trắc nghiệm ' . $exception->getMessage() . PHP_EOL;
            }

        } else {
            try {
                foreach ($item_question->quest_answer as $quest_answer) {
                    $answerItemRepositoryEloquent->create([
                        'quiz_id' => $quiz->_id,
                        'content' => $quest_answer->answer_name,
                        'is_correct' => (int)$quest_answer->answer_iscorrect,
                    ]);
                }
            } catch (\Exception $exception) {
                echo 'Error : Trắc nghiệm ' . $exception->getMessage() . PHP_EOL;
            }
        }
        $regex = '/Lời giải<\/p>(.*)<\/div>/m';
        $match = $this->regex($regex, $item_question->content);
        if (!empty($match) && isset($match[0][1])) {
            $quiz->answer = $match[0][1];
            $quiz->save();
        }
        return $quiz;
    }

    function regex($regex, $content)
    {
        preg_match_all($regex, $content, $matches, PREG_SET_ORDER, 0);
        return $matches;
    }
}
