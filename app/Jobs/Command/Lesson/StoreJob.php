<?php

namespace App\Jobs\Command\Lesson;

use App\Entities\Lesson;
use App\Jobs\Job;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\LessonRepositoryEloquent;
use App\Repositories\SubjectRepositoryEloquent;

class StoreJob extends Job
{
    protected $obj_lesson;
    protected $chapter_id;

    /**
     * StoreJob constructor.
     * @param $chapter_id
     * @param $obj_lesson
     */
    public function __construct($obj_lesson, $chapter_id)
    {
        $this->obj_lesson = $obj_lesson;
        $this->chapter_id = $chapter_id;
    }

    /**
     * @param SubjectRepositoryEloquent $repositoryEloquent
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(LessonRepositoryEloquent $repositoryEloquent)
    {

        $lesson = Lesson::where('name', $this->obj_lesson->lession_name)->where('chapter_id', $this->chapter_id)->first();
        if (!is_object($lesson)) {

            $lesson = $repositoryEloquent->create([
                'name' => $this->obj_lesson->lession_name,
                'sub_title' => $this->obj_lesson->lession_subtitle,
                'slug' => str_slug($this->obj_lesson->lession_name, '-'),
                'chapter_id' => $this->chapter_id,
            ]);
        }

        return $lesson;
    }
}
