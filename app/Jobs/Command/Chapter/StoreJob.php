<?php

namespace App\Jobs\Command\Chapter;

use App\Entities\Chapter;
use App\Jobs\Job;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\ChapterRepositoryEloquent;
use App\Repositories\SubjectRepositoryEloquent;
use Illuminate\Support\Str;

class StoreJob extends Job
{
    protected $obj_chapter;
    protected $course_id;
    protected $subject_id;

    /**
     * StoreJob constructor.
     * @param $obj_subject
     * @param $course_id
     * @param $subject_id
     */
    public function __construct($obj_chapter, $course_id, $subject_id)
    {
        $this->obj_chapter = $obj_chapter;
        $this->course_id = $course_id;
        $this->subject_id = $subject_id;
    }

    /**
     * @param SubjectRepositoryEloquent $repositoryEloquent
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(ChapterRepositoryEloquent $repositoryEloquent)
    {

        $chapter = Chapter::where('name', Str::upper($this->obj_chapter->topic_name))->first();
        if (!is_object($chapter)) {
            $chapter = $repositoryEloquent->create([
                'heading' => $this->obj_chapter->heading,
                'name' => Str::upper($this->obj_chapter->topic_name),
                'short_name' => Str::upper($this->obj_chapter->topic_shortname),
                'slug' => str_slug(Str::upper($this->obj_chapter->topic_name), '-'),
                'course_id' => $this->course_id,
                'subject_id' => $this->subject_id,
            ]);
        }
        return $chapter;
    }
}
