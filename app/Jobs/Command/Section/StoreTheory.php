<?php

namespace App\Jobs\Command\Section;

use App\Entities\Section;
use App\Jobs\Job;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\SectionRepositoryEloquent;
use App\Repositories\SubjectRepositoryEloquent;

class StoreTheory extends Job
{
    protected $content;
    protected $chapter_id;
    protected $lesson_id;
    protected $type;

    /**
     * StoreJob constructor.
     * @param $content
     * @param $chapter_id
     * @param $lesson_id
     * @param $type
     */
    public function __construct($content, $chapter_id, $lesson_id, $type)
    {
        $this->content = $content;
        $this->chapter_id = $chapter_id;
        $this->lesson_id = $lesson_id;
        $this->type = $type;
    }

    /**
     * @param SubjectRepositoryEloquent $repositoryEloquent
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(SectionRepositoryEloquent $repositoryEloquent)
    {
        $title = 'Lí thuyết';
        $section = Section::where('name', $title)->where('lesson_id', (int)$this->lesson_id)->where('chapter_id', (int)$this->chapter_id)->first();
        if (!is_object($section)) {
            $section = $repositoryEloquent->create([
                'name' => $title,
                'slug' => str_slug($title, '-'),
                'lesson_id' => (int)$this->lesson_id,
                'chapter_id' => (int)$this->chapter_id,
                'description' => '',
                'question' => '',
                'type' => $this->type,
                'answer' => '',
                'content' => $this->content,
            ]);

        }
        return $section;
    }
}
