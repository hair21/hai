<?php

namespace App\Jobs\Command\Section;

use App\Jobs\Job;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\SectionRepositoryEloquent;
use App\Repositories\SubjectRepositoryEloquent;

class StoreExercise extends Job
{
    protected $obj_section;
    protected $answer;
    protected $chapter_id;
    protected $lesson_id;
    protected $type;

    /**
     * StoreJob constructor.
     * @param $content
     * @param $chapter_id
     * @param $lesson_id
     * @param $type
     * @param $answer
     * @param $obj_section
     */
    public function __construct($obj_section, $answer, $chapter_id, $lesson_id, $type)
    {
        $this->obj_section = $obj_section;
        $this->answer = $answer;
        $this->chapter_id = $chapter_id;
        $this->lesson_id = $lesson_id;
        $this->type = $type;
    }

    /**
     * @param SubjectRepositoryEloquent $repositoryEloquent
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(SectionRepositoryEloquent $repositoryEloquent)
    {
        $item_section = $this->obj_section;
        $section = $repositoryEloquent->create([
            'name' => $item_section->faq_title,
            'slug' => str_slug($item_section->faq_title, '-'),
            'lesson_id' => (int)$this->lesson_id,
            'chapter_id' => (int)$this->chapter_id,
            'description' => $item_section->faq_desc,
            'question' => $item_section->faq_content,
            'type' => $this->type,
            'answer' => $this->answer,
        ]);

        return $section;
    }
}
