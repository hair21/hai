<?php

namespace App\Jobs\Command\Subject;

use App\Entities\Subject;
use App\Jobs\Job;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\SubjectRepositoryEloquent;

class StoreJob extends Job
{
    protected $obj_subject;

    /**
     * StoreJob constructor.
     * @param $obj_subject
     */
    public function __construct($obj_subject)
    {
        $this->obj_subject = $obj_subject;
    }

    /**
     * @param SubjectRepositoryEloquent $repositoryEloquent
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(SubjectRepositoryEloquent $repositoryEloquent)
    {
        $subject = Subject::where('name', $this->obj_subject->subject_name)->first();
        if (!is_object($subject)) {
            $subject = $repositoryEloquent->create([
                'name' => $this->obj_subject->subject_name,
                'slug' => str_slug($this->obj_subject->subject_name, '-'),
            ]);
        }
        return $subject;
    }
}
