<?php

namespace App\Jobs\Admin\Subject;

use App\Jobs\Job;
use App\Repositories\AdminRepositoryEloquent;
use App\Repositories\CategoryRepositoryEloquent;
use Illuminate\Http\Request;

class StoreJob extends Job
{
    protected $request;

    /**
     * StoreJob constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param CategoryRepositoryEloquent $categoryRepository
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(CategoryRepositoryEloquent $categoryRepository)
    {
        $data = array_merge(
            $this->request->only(['name', 'description','type']),
            [
                'status' => $this->request->get('status') == 'on' ? 1 : 0,
                'slug' => str_slug($this->request->name, '-'),
            ]
        );
        $categoryRepository->create($data);
    }
}
