<?php

namespace App\Jobs\Admin\Subject;

use App\Entities\CategoriesCourse;
use App\Entities\Category;
use App\Jobs\Job;
use Illuminate\Http\Request;

class UpdateJob extends Job
{
    protected $request;
    protected $category;

    /**
     * StoreJob constructor.
     * @param Request $request
     */
    public function __construct(Request $request, Category $category)
    {
        $this->request = $request;
        $this->category = $category;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = array_merge(
            $this->request->only(['name', 'code', 'description', 'type']),
            [
                'status' => $this->request->get('status') == 'on' ? 1 : 0,
                'slug' => str_slug($this->request->name, '-'),
            ]
        );
        $this->category->update($data);
    }
}
