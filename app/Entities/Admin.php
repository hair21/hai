<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

//use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class Admin.
 *
 * @package namespace App\Entities;
 */
class Admin extends Model implements Transformable
{
    use TransformableTrait;
    use HybridRelations;

    protected $connection = 'mysql';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
    protected $primaryKey = '_id';
//    public function courses()
//    {
//        return $this->hasMany(Course::class, 'admin_id');
//    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'admin_id');
    }
}
