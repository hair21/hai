<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Lesson.
 *
 * @package namespace App\Entities;
 */
class Lesson extends Model implements Transformable
{
    use HybridRelations;
    use TransformableTrait;
    protected $connection = 'mysql';
    protected $primaryKey = '_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'chapter_id', 'sub_title','id_clone','status'];

    protected $appends = ['sgk', 'sbt', 'lithuyet', 'tracnghiem'];

    public function getSgkAttribute()
    {
        return '<a href="' . route('admin.lessons.list', $this->_id) . '?type=2">' . $this->sections()->where('type', 2)->count() . '</a>';
    }

    public function getSbtAttribute()
    {
        return '<a href="' . route('admin.lessons.list', $this->_id) . '?type=1">' . $this->sections()->where('type', 1)->count() . '</a>';
    }

    public function getLithuyetAttribute()
    {

        return Section::where('lesson_id', (int)$this->_id)->where('type', 3)->exists() ? '<a href="' . route('admin.lessons.lithuyet', $this->_id) . '">Xem lí thuyết</a>' : '';
    }

    public function getTracnghiemAttribute()
    {
        return '<a href="' . route('admin.lessons.quiz', $this->_id) . '">' . $this->quizzes()->count() . '</a>';
    }

    public function sections()
    {
        return $this->hasMany(Section::class, 'lesson_id');
    }

    public function quizzes()
    {
        return $this->hasMany(Quiz::class, 'lesson_id');
    }

    public function delete()
    {
        $this->sections()->delete();
        return parent::delete();
    }
}
