<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AnswerItem.
 *
 * @package namespace App\Entities;
 */
class AnswerItem extends Model implements Transformable
{
    use TransformableTrait;
    protected $connection = 'mongodb';
    protected $collection = 'answer_items';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['quiz_id', 'content', 'is_correct'];

}
