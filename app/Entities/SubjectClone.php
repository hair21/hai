<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class SubjectClone.
 *
 * @package namespace App\Entities;
 */
class SubjectClone extends Model implements Transformable
{
    use TransformableTrait;
    protected $connection = 'mysql';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['class', 'id_clone','status','name'];

}
