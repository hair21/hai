<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Quiz.
 *
 * @package namespace App\Entities;
 */
class Quiz extends Model implements Transformable
{
    use TransformableTrait;
    protected $connection = 'mongodb';
    protected $collection = 'quizzes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'answer', 'quest_id','lesson_id'];

    public function answer()
    {
        return $this->hasMany(AnswerItem::class, 'quiz_id');
    }


}
