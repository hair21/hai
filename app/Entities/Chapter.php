<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Chapter.
 *
 * @package namespace App\Entities;
 */
class Chapter extends Model implements Transformable
{
    use HybridRelations;
    use TransformableTrait;
    protected $connection = 'mysql';
    protected $primaryKey = '_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'short_name', 'slug', 'description', 'course_id', 'subject_id', 'heading', 'id_clone', 'status'];

    public function lessons()
    {
        return $this->hasMany(Lesson::class, 'chapter_id');
    }

    public function sections()
    {
        return $this->hasMany(Section::class, 'chapter_id');
    }

    public function delete()
    {
        $this->lessons()->delete();
        $this->sections()->delete();
        return parent::delete();
    }
}
