<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Subject.
 *
 * @package namespace App\Entities;
 */
class Subject extends Model implements Transformable
{
    use TransformableTrait;
    protected $connection = 'mysql';
    protected $primaryKey = '_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'course_id','id_clone','status'];


    public function chapters()
    {
        return $this->hasMany(Chapter::class, 'subject_id');
    }

}
