<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

//use Illuminate\Database\Eloquent\Model;

/**
 * Class Section.
 *
 * @package namespace App\Entities;
 */
class Section extends Model implements Transformable
{
    use TransformableTrait;

    protected $connection = 'mongodb';
    protected $collection = 'sections';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['lesson_id', 'name', 'question', 'description', 'chapter_id', 'answer', 'type','content'];


    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class, 'chapter_id');
    }
}
