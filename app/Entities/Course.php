<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

//use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class Course.
 *
 * @package namespace App\Entities;
 */
class Course extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'mysql';
    protected $primaryKey = '_id';
//    protected $collection = 'courses';
    protected $fillable = ['name', 'slug', 'description'];

//    public function admin()
//    {
//        return $this->belongsTo(Admin::class);
//    }
}
