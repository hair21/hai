<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

//use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment.
 *
 * @package namespace App\Entities;
 */
class Comment extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'mongodb';
    protected $collection = 'comments';
    protected $fillable = ['content', 'admin_id', 'comment_id'];
    protected $appends = ['reply'];

    public function admin()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }

    public function repcommnet()
    {
        return $this->hasMany(Comment::class, 'comment_id');
    }

    public function getReplyAttribute()
    {
        return is_null($this->repcommnet) ? false : true;
    }

}
