<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\CourseRepository::class, \App\Repositories\CourseRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AdminRepository::class, \App\Repositories\AdminRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CommnetRepository::class, \App\Repositories\CommnetRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CommentRepository::class, \App\Repositories\CommentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SubjectRepository::class, \App\Repositories\SubjectRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ChapterRepository::class, \App\Repositories\ChapterRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LessonRepository::class, \App\Repositories\LessonRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ClassRepository::class, \App\Repositories\ClassRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SectionRepository::class, \App\Repositories\SectionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SubjectCloneRepository::class, \App\Repositories\SubjectCloneRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuizRepository::class, \App\Repositories\QuizRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AnswerItemRepository::class, \App\Repositories\AnswerItemRepositoryEloquent::class);
        //:end-bindings:
    }
}
