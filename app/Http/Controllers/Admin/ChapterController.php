<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Admin;
use App\Entities\Lesson;
use App\Entities\Section;
use App\Http\Controllers\Controller;
use App\Repositories\ChapterRepositoryEloquent;
use Illuminate\Http\Request;

class ChapterController extends Controller
{
    protected $page;

    public function __construct(ChapterRepositoryEloquent $page)
    {
        $this->page = $page;
    }

    public function create()
    {
        return view('comment.add');
    }

    public function findLesson($id)
    {
        $data['sections'] = Section::where('lesson_id', (int)$id)->get();
//        dd($data);
        return view('section.view')->with($data);
    }

    /*    public function index(Request $request)
        {
    //        dd(Admin::find(2)->comments);
            $data['admins'] = Admin::pluck('name', '_id')->toArray();
            $data['comments'] = Comment::whereNull('comment_id')->get();
    //        dd($data);
            return view('comment.view')->with($data);
        }

        public function store(Request $request)
        {
            $data = array_merge($request->only('content'), [
                'admin_id' => (int)$request->admin_id,
                'comment_id' => $request->comment_id,
            ]);
            Comment::create($data);
            return redirect()->route('admin.comments.index');
        }*/

    public function edit(Request $request, $id)
    {
        $data['lessons'] = Lesson::where('chapter_id', (int)$id)->get();
        $data['sections'] = Section::where('chapter_id', (int)$id)->whereNull('lesson_id')->get();
        return view('chapter.edit')->with($data);
    }

    public function lithuyet($lesson_id)
    {
        $data['section'] = Section::where('lesson_id', (int)$lesson_id)->where('type', 3)->first();
        return view('section.lithuyet')->with($data);
    }

    public function show(Request $request, $id)
    {
        $data['courses'] = Admin::where('_id', $id)->first()->courses;
        return view('comment.list')->with($data);
    }

    public function destroy($id)
    {
        $this->page->delete($id);
        return back();
    }
}
