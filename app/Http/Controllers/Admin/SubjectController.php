<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Admin;
use App\Entities\Chapter;
use App\Entities\Course;
use App\Entities\Subject;
use App\Repositories\SubjectRepositoryEloquent;
use Illuminate\Http\Request;

class SubjectController extends BaseController
{
    protected $page;

    public function __construct(SubjectRepositoryEloquent $page)
    {
        $this->page = $page;
    }

    public function create()
    {
        $data['admins'] = Admin::pluck('name', 'id')->toArray();
        return view('course.add')->with($data);
    }

    public function index(Request $request)
    {
        $data['courses'] = Course::all();
        return view('course.view')->with($data);
    }

    public function store(Request $request)
    {
        Course::create([
            'name' => $request->name,
            'slug' => str_slug($request->name, '-'),
            'description' => $request->description,
            'admin_id' => (int)$request->admin_id,
        ]);
        return redirect()->route('admin.courses.index');
    }

    public function listchap(Request $request, $course_id, $subject_id)
    {
        $data['chapters'] = Chapter::where('subject_id', $subject_id)->where('course_id', $course_id)->get();
        return view('subject.list')->with($data);
    }

    public function list(Request $request, $id)
    {
        $data['subjects'] = Subject::all();
        $data['course'] = Course::find($id);
        return view('course.list')->with($data);
    }


    public function update(Request $request, $id)
    {
        Course::where('_id', $id)->update([
            'name' => $request->name,
            'slug' => str_slug($request->name, '-'),
            'description' => $request->description,
            'admin_id' => (int)$request->admin_id,
        ]);
        return redirect()->route('admin.courses.index');
    }

    public function destroy($id)
    {
        $this->page->delete($id);
        return redirect()->route('admin.courses.index');
    }
}
