<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Bill;
use App\Entities\CategoriesCourse;
use App\Entities\Category;
use App\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AbstractController;
use Exception;
use App\Entities\Course;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

abstract class BaseController extends AbstractController
{
    /**
     * Default message
     * @var array
     */
    protected $e = [
        'status' => true,
        'message' => null,
    ];


    protected function doRequest(
        Request $request,
        callable $callback,
        $successMessage = null
    )
    {
        DB::beginTransaction();
        try {
            $code = 200;
            $this->e['message'] = $successMessage ?: 'Thành công';
            if (is_callable($callback)) {
                $result = call_user_func_array($callback, [$request]);
                if (is_null($result)) {
                    $code = 300;
                    $this->e['message'] = 'null';
                }
            }
            DB::commit();
        } catch (Exception $e) {
            $this->e['status'] = false;
            $this->e['message'] = $successMessage ?: $e->getMessage();
        }

        return $this->e['status']
            ? responder()->success([
                'message' => $this->e['message'],
                'result' => $result
            ])->respond($code)
            : responder()->error(0, $this->e['message'])->respond(Response::HTTP_PAYMENT_REQUIRED);
    }


}
