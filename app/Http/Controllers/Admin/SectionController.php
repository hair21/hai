<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Admin;
use App\Entities\Quiz;
use App\Entities\Section;
use App\Http\Controllers\Controller;
use App\Repositories\SectionRepositoryEloquent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Spatie\Browsershot\Browsershot;
use VerumConsilium\Browsershot\Facades\PDF;

class SectionController extends Controller
{
    protected $page;

    public function __construct(SectionRepositoryEloquent $page)
    {
        $this->page = $page;
    }

    public function create()
    {
        return view('comment.add');
    }

    public function findLesson(Request $request, $id)
    {
        if ($request->has('type')) {
            $data['sections'] = Section::where('lesson_id', (int)$id)->where('type', (int)$request->type)->get();
        } else {
            $data['sections'] = Section::where('lesson_id', (int)$id)->get();
        }

//        dd($data);
        return view('section.view')->with($data);
    }

    public function quiz($id)
    {
        Session::forget('exam');
        $collection = collect(Quiz::where('lesson_id', (int)$id)->get());
        $shuffled = $collection->shuffle();
        $shuffled->all();
        Session::put('exam', $shuffled);
        $data['quizzes'] = $shuffled;
        $data['id_lesson'] = $id;
        return view('section.quiz')->with($data);
    }

    public function download(Request $request, $id)
    {
//        if ($request->ajax()) {
//            $pdf = App::make('dompdf.wrapper');
////            dd($request->all());
//            $pdf->loadHTML($request->html);
//            return $pdf->save('abc234123.pdf');
////            Session::put('exam', $request->html);
////            dd(Session::get('exam'));
//        } else {
//           Session::get('exam');
//            $pdf = App::make('dompdf.wrapper');
//            $pdf->loadHTML(Session::get('exam'));
//            return $pdf->stream();
//        }
//        dd(1);
        $exam = Session::get('exam');
        $data['quizzes'] = $exam;
        $data['id_lesson'] = $id;

//        return PDF::loadHtml('<h1>Awesome PDF</h1>')
//            ->setNodeBinary('C:\Windows\System32\cmd.exe node')
//            ->storeAs('pdfs/', 'google.pdf');
        return Browsershot::url('https://www.google.com')
            ->setNodeBinary('C:\Windows\System32\cmd.exe node')
            ->noSandbox()->format('a4')->save('C:\xampp\htdocs\db-mongo\public\demo.pdf');
//        loadHtml('<h1>Awesome PDF</h1>')
//          ->setNodeBinary('C:\Windows\System32\cmd.exe node')
//            ->inline();
//        ChromePDF::loadView('section.quiz',$data)->inline();
//        ChromePDF::loadView('section.quiz', $data)->inline();
//            ->size('a4')
//            ->landscape()
//            ->download("invoice.pdf");

//        $pdfStoredPath = PDF::loadUrl('https://google.com')
//            ->setNodeBinary('C:/Program Files/nodejs/node.exe')
//            ->setNpmBinary('C:\Program Files\nodejs\npm.CMD')
//            ->inline();
//        $exam = Session::get('exam');
//        $data['quizzes'] = $exam;
//        $data['id_lesson'] = $id;
//        PDF::loadView('section.quiz',$data)->inline();
    }

    public function edit(Request $request, $id)
    {
        $data['section'] = Section::where('_id', $id)->first();
//        dd( $data['section']->content);
        return view('section.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        Admin::where('_id', $id)->update([
            'name' => $request->name,
        ]);
        return redirect()->route('comment.comments.index');
    }

    public function list($id)
    {
        $data['sections'] = Section::where('lesson_id', (int)$id)->get();
        return view('section.view')->with($data);
    }

    public function destroy($id)
    {
        $this->page->delete($id);
        return back();
    }
}
