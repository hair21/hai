<?php

namespace App\Http\Controllers\Admin;

use App\Entities\SubjectClone;
use App\Http\Controllers\Controller;
use App\Repositories\LessonRepositoryEloquent;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    protected $page;

    public function __construct(LessonRepositoryEloquent $page)
    {
        $this->page = $page;
    }

    public function create()
    {
        return view('subject_clone.add');
    }


    public function index(Request $request)
    {
        $data['clones'] = SubjectClone::all()->groupBy('class');
        return view('subject_clone.view')->with($data);
    }

    public function store(Request $request)
    {
        $data = array_merge($request->only('class', 'id_clone', 'name'), [
            'status' => 0
        ]);
        SubjectClone::create($data);
        return redirect()->route('admin.clones.index');
    }

    public function edit(Request $request, $id)
    {
        $data['clone'] = SubjectClone::where('id', $id)->first();
        return view('subject_clone.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        SubjectClone::where('id', $id)->update([
            'id_clone' => $request->id_clone,
        ]);
        return redirect()->route('admin.clones.index');
    }
//
//    public function show(Request $request, $id)
//    {
//        $data['courses'] = Admin::where('_id', $id)->first()->courses;
//        return view('comment.list')->with($data);
//    }
//
    public function destroy($id)
    {
        $this->page->delete($id);
        return back();
    }
}
