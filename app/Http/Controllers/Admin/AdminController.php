<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Admin;
use App\Http\Controllers\Controller;
use App\Repositories\AdminRepositoryEloquent;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $page;

    public function __construct(AdminRepositoryEloquent $page)
    {
        $this->page = $page;
    }

    public function create()
    {
        return view('admin.add');
    }

    public function index(Request $request)
    {
        $data['admins'] = Admin::all();
        return view('admin.view')->with($data);
    }

    public function store(Request $request)
    {
        Admin::create([
            'name' => $request->name,
        ]);
        return redirect()->route('admin.admins.index');
    }

    public function edit(Request $request, $id)
    {
        $data['admin'] = Admin::where('_id', $id)->first();
        return view('admin.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        Admin::where('_id', $id)->update([
            'name' => $request->name,
        ]);
        return redirect()->route('admin.admins.index');
    }

    public function show(Request $request, $id)
    {
        $data['courses'] = Admin::where('_id', $id)->first()->courses;
        return view('admin.list')->with($data);
    }

    public function destroy($id)
    {
        $this->page->delete($id);
        return redirect()->route('admin.admins.index');
    }
}
