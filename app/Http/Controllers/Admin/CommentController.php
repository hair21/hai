<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Admin;
use App\Entities\Comment;
use App\Http\Controllers\Controller;
use App\Repositories\AdminRepositoryEloquent;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    protected $page;

    public function __construct(AdminRepositoryEloquent $page)
    {
        $this->page = $page;
    }

    public function create()
    {
        return view('comment.add');
    }

    public function index(Request $request)
    {
//        dd(Admin::find(2)->comments);
        $data['admins'] = Admin::pluck('name', '_id')->toArray();
        $data['comments'] = Comment::whereNull('comment_id')->get();
//        dd($data);
        return view('comment.view')->with($data);
    }

    public function store(Request $request)
    {
        $data = array_merge($request->only('content'), [
            'admin_id' => (int)$request->admin_id,
            'comment_id' => $request->comment_id,
        ]);
        Comment::create($data);
        return redirect()->route('admin.comments.index');
    }

    public function edit(Request $request, $id)
    {
        $data['comment'] = Admin::where('_id', $id)->first();
        return view('comment.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        Admin::where('_id', $id)->update([
            'name' => $request->name,
        ]);
        return redirect()->route('comment.comments.index');
    }

    public function show(Request $request, $id)
    {
        $data['courses'] = Admin::where('_id', $id)->first()->courses;
        return view('comment.list')->with($data);
    }

    public function destroy($id)
    {
        $this->page->delete($id);
        return redirect()->route('comment.comments.index');
    }
}
