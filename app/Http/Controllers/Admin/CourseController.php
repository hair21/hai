<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Admin;
use App\Entities\Course;
use App\Entities\Subject;
use App\Repositories\CourseRepositoryEloquent;
use Illuminate\Http\Request;

class CourseController extends BaseController
{
    protected $page;

    public function __construct(CourseRepositoryEloquent $page)
    {
        $this->page = $page;
    }

    public function create()
    {
        $data['admins'] = Admin::pluck('name', 'id')->toArray();
        return view('course.add')->with($data);
    }

    public function index(Request $request)
    {
        $data['courses'] = Course::all();
        return view('course.view')->with($data);
    }

    public function store(Request $request)
    {
        Course::create([
            'name' => $request->name,
            'slug' => str_slug($request->name, '-'),
            'description' => $request->description,
            'admin_id' => (int)$request->admin_id,
        ]);
        return redirect()->route('admin.courses.index');
    }

    public function edit(Request $request, $id)
    {
        $data['admins'] = Admin::pluck('name', '_id')->toArray();
        $data['course'] = Course::where('_id', $id)->first();
        return view('course.edit')->with($data);
    }

    public function list(Request $request, $id)
    {
        $data['subjects'] = Subject::where('course_id', $id)->get();
        return view('course.list')->with($data);
    }

    public function update(Request $request, $id)
    {
        Course::where('_id', $id)->update([
            'name' => $request->name,
            'slug' => str_slug($request->name, '-'),
            'description' => $request->description,
            'admin_id' => (int)$request->admin_id,
        ]);
        return redirect()->route('admin.courses.index');
    }

    public function destroy($id)
    {
        $this->page->delete($id);
        return redirect()->route('admin.courses.index');
    }
}
