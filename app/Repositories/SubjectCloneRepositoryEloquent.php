<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SubjectCloneRepository;
use App\Entities\SubjectClone;
use App\Validators\SubjectCloneValidator;

/**
 * Class SubjectCloneRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SubjectCloneRepositoryEloquent extends BaseRepository implements SubjectCloneRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SubjectClone::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
