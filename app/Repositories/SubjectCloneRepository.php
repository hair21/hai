<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SubjectCloneRepository.
 *
 * @package namespace App\Repositories;
 */
interface SubjectCloneRepository extends RepositoryInterface
{
    //
}
