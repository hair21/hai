<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuizRepository.
 *
 * @package namespace App\Repositories;
 */
interface QuizRepository extends RepositoryInterface
{
    //
}
