<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AnswerItemRepository.
 *
 * @package namespace App\Repositories;
 */
interface AnswerItemRepository extends RepositoryInterface
{
    //
}
