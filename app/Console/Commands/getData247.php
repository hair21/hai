<?php

namespace App\Console\Commands;

use App\Entities\Section;
use App\Jobs\Command\Chapter\StoreJob as StoreChapter;
use App\Jobs\Command\Course\StoreJob as StoreCourse;
use App\Jobs\Command\Lesson\StoreJob as StoreLesson;
use App\Jobs\Command\Quiz\StoreJob as StoreQuiz;
use App\Jobs\Command\Section\StoreExercise;
use App\Jobs\Command\Section\StoreTheory;
use App\Jobs\Command\Subject\StoreJob as StoreSubject;
use Illuminate\Console\Command;

class getData247 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '247:get';
    protected $base_url = 'http://panel.etado.vn/hoc247/';
    protected $heading = '';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $type = [
//        1 => 'sachbaitap',
//        2 => 'sgk',
//        3 => 'lithuyet',
        4 => 'tracnghiem',
    ];
///getcomposecontent.php?lession_id=2831

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i = 6; $i <= 12; $i++) {
            $url = $this->base_url . '?gradecode=lop-' . $i;
//            THEM LOP HOC
            $course = StoreCourse::dispatchNow($i);
            $data_subject = $this->getCurl($url);
            foreach ($data_subject->data as $item_subject) {
//                    THEM MON
                $subject = StoreSubject::dispatchNow($item_subject);
                $url_subject = $this->base_url . 'get-chuong-bai.php?subjectcode=' . $item_subject->subject_code . '&gradecode=lop-' . $i;
                $data_chapter = $this->getCurl($url_subject);


                if ($data_subject->success) {
                    foreach ($data_chapter->data as $item_chapter) {
                        $this->heading = ($this->heading != $item_chapter->topic_heading) ? $item_chapter->topic_heading : '';
                        $item_chapter->heading = $this->heading;
//                            THEM CHUONG
                        $chapter = StoreChapter::dispatchNow($item_chapter, $course->_id, $subject->_id);
                        foreach ($item_chapter->lession as $item_lesson) {
//                                THEM BAI HOC
                            $lesson = StoreLesson::dispatchNow($item_lesson, $chapter->_id);
                            foreach ($this->type as $type => $name) {
                                if ($type == 4) {
                                    $url_quiz = $this->base_url . 'getquizdetail.php?lession_id=' . $item_lesson->lession_id;
                                    $data_quiz = $this->getCurl($url_quiz);
                                    try {
                                        foreach ($data_quiz->data->arrdata as $item_question) {
//                                                THEM CAU HOI TRAC NGHIEM
                                            $quiz = StoreQuiz::dispatchNow($item_question, $lesson->_id);
                                            echo '|Môn| : ' . $subject->name . ' |Lớp| : ' . $i . ' - |Chương| : ' . $chapter->name . ' - |Bài| : ' . $lesson->name . ' |Trắc nghiệm| : ' . $quiz->question . PHP_EOL;
                                        }
                                    } catch (\Exception $exception) {
                                        echo 'Error : ' . $name . $exception->getMessage() . PHP_EOL;
                                    }


                                } elseif ($type == 3) {
                                    $url_theory = $this->base_url . 'getlessiondetail.php?lessionid=' . $item_lesson->lession_id;
                                    $data_theory = $this->getCurl($url_theory);
                                    if ($data_theory->success) {
//                                            THEM PHAN LI THUYET BAI HOC
                                        $section = StoreTheory::dispatchNow($data_theory->data->content, $chapter->_id, $lesson->_id, $type);
                                    } else {
                                        echo 'Error : ' . $name . PHP_EOL;
                                    }
                                } else {
                                    $url_lesson = $this->base_url . 'giaibt.php?lessionid=' . $item_lesson->lession_id . '&type=' . $name;
                                    $data_lesson = $this->getCurl($url_lesson);

                                    try {
                                        foreach ($data_lesson->data->content as $item_section) {
                                            $section = Section::where('name', $item_section->faq_title)->where('chapter_id', (int)$chapter->_id)->first();
                                            if (!is_object($section)) {
                                                $url_section = $this->base_url . 'giaibt-chitiet.php?faq_id=' . $item_section->faq_id;
                                                $data_section = $this->getCurl($url_section);
                                                if ($data_section->success) {
 //                                            THEM SACH BAI TAP/SACH GIAO KHOA
                                                    $section = StoreExercise::dispatchNow($item_section, $data_section->data->content[0]->faqans_content, $chapter->_id, $lesson->_id, $type);
                                                    echo '|Môn| : ' . $subject->name . ' |Lớp| : ' . $i . ' - |Chương| : ' . $chapter->name . ' - |Bài| : ' . $lesson->name . ' |' . $name . '| : ' . $section->name . PHP_EOL;
                                                } else {
                                                    echo 'Error : ' . $name . PHP_EOL;
                                                }
                                            }

                                        }
                                    } catch (\Exception $exception) {
//
                                        echo 'Error : |' . $item_lesson->lession_id . '|' . $lesson->name . '|' . $exception->getMessage() . PHP_EOL;
                                    }

                                }
                            }

                        }

                    }
                } else {
                    echo 'Error : Môn học' . PHP_EOL;
                }
            }
        }
    }

    public function getCurl($url)
    {


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: __cfduid=d2b672ec262991b4bc205cdccbd5795441589187937"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response);
    }

    public function regex($regex, $content)
    {
        preg_match_all($regex, $content, $matches, PREG_SET_ORDER, 0);
        return $matches;
    }
}
