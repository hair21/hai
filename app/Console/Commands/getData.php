<?php

namespace App\Console\Commands;

use App\Entities\AnswerItem;
use App\Entities\Chapter;
use App\Entities\Course;
use App\Entities\Lesson;
use App\Entities\Quiz;
use App\Entities\Section;
use App\Entities\Subject;
use App\Entities\SubjectClone;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class getData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:get';
    protected $base_url = 'http://panel.etado.vn/loigiaihay/loigiaihay/';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data loigiaihay';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $data['quizzes'] = Quiz::
//        where('lesson_id', (int)$id)->
//        orderBy(DB::raw('RAND()'))->get()->random();
//        $course = Course::pluck('_id','name')->random(2);
//random query
//        $course = Quiz::where('lesson_id', (int)3822)->get();
//        $collection = collect($course);
//        $shuffled = $collection->shuffle();
//        $shuffled->all();
        dd(AnswerItem::truncate());


        $clones = SubjectClone::all();
        foreach ($clones as $clone) {
            if ($clone->status == 0) {
                $class = $clone->class;
                $course = Course::where('name', 'Lớp ' . $clone->class)->first();
                if (!is_object($course)) {
                    $course = Course::create([
                        'name' => 'Lớp ' . $class,
                        'slug' => str_slug('Lớp ' . $class, '-'),
                    ]);
                }
//            echo '|Lớp| : ' . $course->name . PHP_EOL;

                $subject = Subject::where('name', $clone->name)->where('course_id', $course->_id)->first();
                if (!is_object($subject)) {
                    $subject = Subject::create([
                        'name' => $clone->name,
                        'slug' => str_slug($clone->name . '-Lớp-' . $class, '-'),
                        'course_id' => $course->_id
                    ]);
                }
//                echo '|Môn| : ' . $subject->name . ' |Lớp| ' . $class . PHP_EOL;

                $url_get_chapter = 'list_chuong.php?item_id=' . $clone->id_clone;
                $data_subject = $this->getCurl($url_get_chapter);

                if (!isset($data_subject->success)) {
                    foreach ($data_subject->listEvents as $item_chapter) {
//                        dd();
//                    $check_name = $this->regex($key, $chapter_name);
                        $chapter = Chapter::where('name', Str::upper($item_chapter->title))->where('course_id', $course->_id)->where('subject_id', $subject->_id)->first();
                        if (!is_object($chapter)) {
                            $chapter = Chapter::create([
                                'name' => Str::upper($item_chapter->title),
                                'slug' => str_slug(Str::upper($item_chapter->title), '-'),
                                'course_id' => $course->_id,
                                'subject_id' => $subject->_id,
                                'description' => $item_chapter->introtext,
                            ]);
                        }
//                        echo '|Môn| : ' . $subject->name . ' |Lớp| : ' . $class . ' - |Chương| : ' . $chapter->name . PHP_EOL;

                        if (count($item_chapter->subItems) > 0) {
                            foreach ($item_chapter->subItems as $item_lesson) {

                                $lesson = Lesson::where('name', $item_lesson->title)->where('chapter_id', $chapter->_id)->first();

                                if (!is_object($lesson)) {

                                    $lesson = Lesson::create([
                                        'name' => $item_lesson->title,
                                        'slug' => str_slug($item_lesson->title, '-'),
                                        'chapter_id' => $chapter->_id,
                                        'description' => $item_lesson->introtext,
                                    ]);
                                }

//                            echo '|Môn| : ' . $subject->name . ' |Lớp| : ' . $class . ' - |Chương| : ' . $chapter->name . ' - |Bài| : ' . $lesson->name . PHP_EOL;

                                $url_list_lesson = 'list_bai.php?item_lession=' . $item_lesson->itemId;
                                $data_lesson = $this->getCurl($url_list_lesson);
                                foreach ($data_lesson->listArticles as $item_section) {
//                            if ($item_section->typeEx == 2) {
                                    $section = Section::where('name', $item_section->title)->where('lesson_id', (int)$lesson->_id)->first();
                                    if (!is_object($section)) {
                                        $url_list_section = 'detail.php?item_detail=' . $item_section->articleId;
                                        $data_section = $this->getCurl($url_list_section);
                                        try {
                                            $section = Section::create([
                                                'name' => $data_section->articleInfo->title,
                                                'slug' => str_slug($data_section->articleInfo->title, '-'),
                                                'lesson_id' => (int)$lesson->_id,
                                                'chapter_id' => (int)$chapter->_id,
                                                'description' => $data_section->articleInfo->introtext,
                                                'content' => $data_section->articleInfo->content,
                                            ]);

                                            echo '|Môn| : ' . $subject->name . ' |Lớp| : ' . $class . ' - |Chương| : ' . $chapter->name . ' - |Bài| : ' . $lesson->name . ' |Content| : ' . $section->name . PHP_EOL;
                                        } catch (\Exception $exception) {
//                                        $section = Section::create([
//                                            'name' => $item_section->title,
//                                            'slug' => str_slug($item_section->title, '-'),
//                                            'lesson_id' => $lesson->_id,
//                                            'description' => $item_section->introtext,
//                                            'content' => $data_section->content,
//                                        ]);
                                            echo $exception->getMessage() . PHP_EOL;
                                        }
                                    } else {

                                        echo '|Môn| : ' . $subject->name . ' |Lớp| : ' . $class . ' - |Chương| : ' . $chapter->name . ' - |Bài| : ' . $lesson->name . ' |Content| : ' . $section->name . ' |EXIST| ' . PHP_EOL;
                                    }
//                            }
                                }

                            }
                        } else {
                            $url_list_lesson = 'list_bai.php?item_lession=' . $item_chapter->itemId;
                            $data_lesson = $this->getCurl($url_list_lesson);
                            foreach ($data_lesson->listArticles as $item_section) {
//                            if ($item_section->typeEx == 2) {
                                $section = Section::where('name', $item_section->title)->where('chapter_id', (int)(int)$chapter->_id)->first();
                                if (!is_object($section)) {
                                    $url_list_section = 'detail.php?item_detail=' . $item_section->articleId;
                                    $data_section = $this->getCurl($url_list_section);
                                    try {
                                        $section = Section::create([
                                            'name' => $data_section->articleInfo->title,
                                            'slug' => str_slug($data_section->articleInfo->title, '-'),
                                            'lesson_id' => null,
                                            'chapter_id' => (int)$chapter->_id,
                                            'description' => $data_section->articleInfo->introtext,
                                            'content' => $data_section->articleInfo->content,
                                        ]);

                                        echo '|Môn| : ' . $subject->name . ' |Lớp| : ' . $class . ' - |Chương| : ' . $chapter->name . ' |Content| : ' . $section->name . PHP_EOL;
                                    } catch (\Exception $exception) {
                                        echo $exception->getMessage() . PHP_EOL;
                                    }
                                } else {

                                    echo '|Môn| : ' . $subject->name . ' |Lớp| : ' . $class . ' - |Chương| : ' . $chapter->name . ' |Content| : ' . $section->name . ' |EXIST| ' . PHP_EOL;
                                }
//                            }
                            }

                        }

                    }
                    $clone->status = 1;
                    $clone->save();
                }

            } else {
                echo '|Môn : ' . $clone->name . '|Lớp :' . $clone->class . '|ID : ' . $clone->id_clone . ' |Done !|' . PHP_EOL;
            }
        }
    }

    public function regex($re, $parameters)
    {
        preg_match_all($re, $parameters, $matches, PREG_SET_ORDER, 0);
        return $matches;
    }

    public function getCurl($url)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
//            CURLOPT_URL => "http://panel.etado.vn/loigiaihay/loigiaihay/list_chuong.php?item_id=387",
            CURLOPT_URL => $this->base_url . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: __cfduid=d2b672ec262991b4bc205cdccbd5795441589187937"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response);
    }

}